<?php

class Constants{

public static $TABLES = array(
      'commentmeta',
    'comments',
    'links',
    'options',
    'postmeta',
    'posts',
    'term_relationships',
    'term_taxomony',
    'termmeta',
    'terms',
    'usermeta',
    'users'
);

public static $NUMBER_OF_ELEMENTS_REQUIRED = array(
    3,
    14,
    12,
    3,
    3,
    22,
    3,
    5,
    3,
    3,
    3,
    10
);

public static $INSERTS = array(
    "INSERT INTO commentmeta (meta_id, comment_id, meta_key, meta_value) VALUES(NULL, ?, ?, ?)",
      "INSERT INTO comments (comment_id, comment_post_id, comment_author, comment_author_email, comment_author_url, comment_author_ip, comment_date, comment_date_gmt, comment_content, comment_karma, comment_approved, comment_agent, comment_type, comment_parent, user_id) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )",
      "INSERT INTO links (link_id, link_url, link_name, link_image, link_target, link_description, link_visible, link_owner, link_rating, link_updated, link_rel, link_notes, link_rss) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
      "INSERT INTO options (option_id, option_name, option_value, autoload) VALUES(NULL, ?, ?, ?)",
    "INSERT INTO postmeta (meta_id, comment_id, meta_key, meta_value) VALUES(NULL, ?, ?, ?)",

    "INSERT INTO posts (id, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
      "INSERT INTO term_relationships (object_id, term_taxonomy_id, term_order) VALUES(?, ?, ?)",
      "INSERT INTO term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count) VALUES(NULL, ?, ?, ?, ?, ?)",
      "INSERT INTO termmeta (meta_id, term_id, meta_key, meta_value) VALUES(NULL, ?, ?, ?)",
      "INSERT INTO terms (term_id, name, slug, term_group) VALUES(NULL, ?, ?, ?)",
      "INSERT INTO usermeta (umeta_id, user_id, meta_key, meta_value) VALUES(NULL, ?, ?, ?)",
      "INSERT INTO users (id, user_login, user_pass, user_nicename, user_email, user_url, user_registered, user_activation_key, user_status, display_name, user_phone) VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

);

public static $BIND_PARAMS = array(
    "isb",
    "issssbbsisssii",
    "ssssssiibsss",
    "sss",
    "iss",
    "ibbssssssssssbbsisissi",
    "",
    "",
    "",
    "",
    "",
    "sssssssiss"
);



public static function findIndexByTableName($tableName){
    return array_search($tableName, Constants::$TABLES);
}

}
?>
