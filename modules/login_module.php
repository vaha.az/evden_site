<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once('mysqli_module.php');
include_once('SQL_PREPARED_STATEMENTS.php');


class Login{
    private $mySqlHelper;
    private $TABLE_NAME ;
    private $INVALID_LOGIN = -1;
    private $INVALID_EMAIL = -2;
    private $INVALID_PASSWORD = -3;
    private $INVALID_LOGIN_MESSAGE = 'Invalid chararacters for LOGIN are used. Please do not use any special characters';
    private $INVALID_EMAIL_MESSAGE = 'Invalid email address. Please use correct  one';
    private $INVALID_PASSWORD_MESSAGE = 'Invalid password. 6 to 15 letters are required';

    private $MIN_PASSWORD_LENGTH = 6;
    private $MAX_PASSWORD_LENGTH = 15;

    function __construct(){
        $TABLE_NAME = Constants::$TABLES[11];
        $this->mySqlHelper = new MySqlHelper($TABLE_NAME);
    }

    function register($userLogin, $userPass, $userEmail, $userPhone = null){
        $errorArr = array();
        $errorCounter = 0;


        if(! $this->checkLogin($userLogin) ){
            $errorArr[$errorCounter++] = $this->INVALID_LOGIN;
        }
        if(! $this->checkEmail($userEmail) ){
            $errorArr[$errorCounter++] = $this->INVALID_EMAIL;
        }

        if($this->checkIfEmailExists($userEmail)){
          print 'Email Exists';
        }
        else{
          print 'Email does not exist<br>';
        }
        if(! $this->checkPassword($userPass)){
            $error[$errorCounter++] = $this->INVALID_PASSWORD;
        }



        if(count($errorArr) <= 0){
            return $errorArr;
        }

        $userLogin = $this->filterVarForMySql($userLogin);
        $arr = array();

        $arr[0] = $userLogin;   //login
        $arr[1] = $userPass;    //password
        $arr[2] = $userLogin;   //displayed name in url
        $arr[3] = $userEmail;   //email
        $arr[4] = $userLogin;   //url (to his profile)
        $arr[5] = $date = date('Y-m-d H:i:s', time()); //current time
        $arr[6] = '12345';      //activation key
        $arr[7] = 500;          //status
        $arr[8] = 'kete';       //display name
        $arr[9] = $userPhone;   //user's phone number

        $this->mySqlHelper->insert($arr);
    }

    function checkLogin($userLogin){
        return preg_match('![^a-z0-9_]!i', $userLogin);
    }

    function checkEmail($userEmail){
        return filter_var($userEmail, FILTER_VALIDATE_EMAIL);
    }

    function checkPassword($userPass){
        $userPass = trim($userPass);
        if(strlen($userPass) < $this->MIN_PASSWORD_LENGTH || strlen($userPass) > $this->MAX_PASSWORD_LENGTH){
            return false;
        }
        return true;
    }

    function isPasswordStrong($userPass){
        return preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $userPass);
    }

    function filterVarForMySql($var){
        return mysql_real_escape_string($var);
    }

    function checkIfEmailExists($email){
      $result =  $this->mySqlHelper->fetch('id', 'user_email', $email );
      if(count($result->fetch_row()) > 0){
        return 1;
      }
      else{
        return 0;
      }
    }
}
?>
