<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once('SQL_PREPARED_STATEMENTS.php');

class MySqlHelper{

    private $mysqli;
    private $USER_NAME = "alver_user";
    private $USER_PASSWORD = "psk15dret";
    private $DEFAULT_DB = "alver";
    private $DEFAULT_TABLE = "users";
    private $HOST = "localhost";
    private $selectedTable;

    function __construct(){
        $this->mysqli = new mysqli($this->HOST, $this->USER_NAME, $this->USER_PASSWORD, $this->DEFAULT_DB);
        $this->selectedTable = $this->DEFAULT_TABLE;
    }

    function __construct1($tableName){
        print $tableName;
        $this->mysqli = new mysqli($this->HOST, $this->USER_NAME, $this->USER_PASSWORD, $this->DEFAULT_DB);
        $this->selectedTable = $tableName;
    }

    function __destruct(){
      $this->mysqli->close();
    }
    function changeTable($tableName){
        $this->selectedTable = $tableName;
    }


    function insert($values){
        if(!is_array($values)){
            throw new Exception("Expected array for insert function");
        }

        $key = Constants::findIndexByTableName($this->selectedTable);
        if($key < 0 || !$key){
            return -1;
        }
        if(count($values) != Constants::$NUMBER_OF_ELEMENTS_REQUIRED[$key]){
            throw new Exception('Number of elements: ' .count($values) . 'in array doesn\'t match number of required elements: ' . Constants::$NUMBER_OF_ELEMENTS_REQUIRED[$key]);
        }

       $statement = $this->mysqli->prepare(Constants::$INSERTS[$key]);


        $params = array();
        $params[] = & Constants::$BIND_PARAMS[$key];

        for($i = 0; $i < count($values); $i++){
            print $values[$i]."\n";
            $params[] = & $values[$i];
        }
        call_user_func_array(array($statement, 'bind_param'), $params);

        if($statement->execute()){
            print 'success';
        }
        else{
            print mysqli_error($this->mysqli);
        }
    }

    function fetch($values, $conditionNames, $conditionValues){
      if(count($conditionNames) != count($conditionValues)){
        throw new Excepetion('Number of conditionNames is not equal to nymber of conditionValues');
      }
      if(count($values) == 0){
        throw new Exception('Number of values must be more than 1 while fetching');
      }
      $preparedValues = ' ';
      for($i = 0; $i < count($values); $i++){
        if($i == count($values) - 1){
          $preparedValues.=( $values[$i] . ' ');
        }
        else{
          $preparedValues.= ($values[$i] . ' ,');
        }
      }

      if(!is_array($values)){
        $preparedValues = ' ' . $values . ' ';
      }
      $preparedConditions = ' ';
      for($i = 0; $i < count($conditionNames); $i++){
        if($i == count($conditionNames) - 1){
          $preparedConditions.= (' ' . $conditionNames[$i] . '=?');
        }
        else{
          $preparedConditions.= (' ' . $conditionNames[$i] . '=? AND');
        }
      }

      if(!is_array($conditionNames)){
        $preparedConditions = $conditionNames . '=?';
      }
        $query = "SELECT " . $preparedValues . "FROM " . $this->selectedTable . " WHERE " . $preparedConditions . " LIMIT 1";
        $statement = $this->mysqli->prepare($query);

        if(!is_array($conditionValues)){
          if(is_string($conditionValues)){
            $statement->bind_param('s', $conditionValues);
          }
          else{
            $statement->bind_param('i', $conditionValues);
          }
        }
        else{
          $preparedBindParams = '';
          for($i = 0; $i < count($conditionValues); $i++){
            if(is_string($conditionValues[$i])){
              $preparedBindParams.='s';
            }
            else{
              $preparedBindParams.='i';
            }
          }
          $params = array();
          $params[] = & preparedBindParams;
          for($i = 0; $i < count($conditionValues); $i++){
              $params[] = & $conditionValues[$i];
          }
          call_user_func_array(array($statement, 'bind_param'), $params);
        }
        $statement->execute();
        $result =  $statement->get_result();
        $statement->close();
        return $result;
    }


    function prepared_stmt($con, $query, $type = "", $param = array()){

    if($stmt = mysqli_prepare($con, $query)){

        $refarg = array($stmt, $type);//First two parameter of mysqli_stmt_bind_param

        foreach ($param as $key => $value)//create array of parameters' references

            $refarg[] =& $param[$key];

        if($type != "")//Jump instruction if argument $type is missing

            call_user_func_array("mysqli_stmt_bind_param", $refarg);//bind parameters with dinamic length

        if(mysqli_stmt_execute($stmt)){//check if execution go fine

            $cols = mysqli_field_count($con);//retrive the number of columns of the resultset

            $result = array_fill(0, $cols, NULL);//create an empty array with the same length of the columns

            $ref = array($stmt);//first argument of mysqli_stmt_bind_result

            foreach ($result as $key => $value)//create array of empty cells' references

                $ref[] =& $result[$key];

            call_user_func_array("mysqli_stmt_bind_result", $ref);//bind results with dinamic length

            return $ref;//return statement and columns references


        }

        else

            return false;



    }

    else

        return false;

}
}

?>
